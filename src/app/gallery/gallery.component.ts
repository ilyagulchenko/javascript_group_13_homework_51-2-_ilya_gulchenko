import {Component} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent{
  password: number = 12345;
  showForm = false;

  showFormAdd(event: Event) {
    event.preventDefault();
    const target: any = <HTMLInputElement>event.target;
    console.log(target.value);
    if (this.password === target.value) {
      this.showForm = true;
    } else {
      alert('Wrong password!')
    }
  }
}
